// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "applicationTitle" : MessageLookupByLibrary.simpleMessage("Flutter List/Details"),
    "laptopDescription" : MessageLookupByLibrary.simpleMessage("A laptop, also called a notebook computer or simply a notebook, is a small, portable personal computer with a \"clamshell\" form factor, having, typically, a thin LCD or LED computer screen mounted on the inside of the upper lid of the \"clamshell\" and an alphanumeric keyboard on the inside of the lower lid. The \"clamshell\" is opened up to use the computer. Laptops are folded shut for transportation, and thus are suitable for mobile use.[1] Its name comes from \"lap\", as it was deemed to be placed for use on a person\'s lap. Although originally there was a distinction between laptops and notebooks, the former being bigger and heavier than the latter, as of 2014, there is often no longer any difference.[2] Laptops are commonly used in a variety of settings, such as at work, in education, in playing games, Internet surfing, for personal multimedia and general home computer use."),
    "laptopTitle" : MessageLookupByLibrary.simpleMessage("Laptop"),
    "listTitle" : MessageLookupByLibrary.simpleMessage("My List"),
    "phoneDescription" : MessageLookupByLibrary.simpleMessage("A telephone, or phone, is a telecommunications device that permits two or more users to conduct a conversation when they are too far apart to be heard directly. A telephone converts sound, typically and most efficiently the human voice, into electronic signals that are transmitted via cables and other communication channels to another telephone which reproduces the sound to the receiving user."),
    "phoneTitle" : MessageLookupByLibrary.simpleMessage("Phone"),
    "tabletDescription" : MessageLookupByLibrary.simpleMessage("A tablet computer, commonly shortened to tablet, is a mobile device, typically with a mobile operating system and LCD touchscreen display processing circuitry, and a rechargeable battery in a single thin, flat package. Tablets, being computers, do what other personal computers do, but lack some I/O capabilities that others have. Modern tablets largely resemble modern smartphones, the only differences being that tablets are relatively larger than smartphones, with screens 7 inches (18 cm) or larger, measured diagonally, and may not support access to a cellular network."),
    "tabletTitle" : MessageLookupByLibrary.simpleMessage("Tablet")
  };
}
