import 'package:flutter/material.dart';

class ListItem {
  String _title;
  IconData _icon;
  String _text;

  ListItem(String title, IconData icon, String text) {
    this._title = title;
    this._icon = icon;
    this._text = text;
  }

  String title() => this._title;
  IconData icon() => this._icon;
  String text() => this._text;
}