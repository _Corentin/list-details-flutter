import 'package:flutter/material.dart';
import 'package:demo_app/models/item.dart';

class MyDetailsPage extends StatefulWidget {

  ListItem _item;

  MyDetailsPage(ListItem item) {
    this._item = item;
  }

  @override
  State<StatefulWidget> createState() => _MyDetailsPageState(this._item);
}

class _MyDetailsPageState extends State<MyDetailsPage> {

  ListItem _item;

  _MyDetailsPageState(ListItem item) {
    this._item = item;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: (){ Navigator.pop(context, true); }),
        title: Text(this._item.title()),
      ),
      body: Padding(
          padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0, bottom: 8.0),
          child: Text(this._item.text())
      )
    );
  }
}