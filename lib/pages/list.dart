import 'package:flutter/material.dart';

import 'package:demo_app/localizations.dart';
import 'package:demo_app/models/item.dart';
import 'package:demo_app/pages/details.dart';
import 'package:demo_app/animations/routes.dart';

class MyListPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _MyListPageState();
}

class _MyListPageState extends State<MyListPage> {

  List<ListItem> _items;

  _MyListPageState() {
    _items = List();
  }

  @override
  Widget build(BuildContext context) {
    _items.clear();
    _items.add(ListItem(AppLocalizations.of(context).phoneTitle, Icons.phone_android, AppLocalizations.of(context).phoneDescription));
    _items.add(ListItem(AppLocalizations.of(context).tabletTitle, Icons.tablet_android, AppLocalizations.of(context).tabletDescription));
    _items.add(ListItem(AppLocalizations.of(context).laptopTitle, Icons.laptop_chromebook, AppLocalizations.of(context).laptopDescription));

    final _widgets = <Widget>[];
    for (var item in _items) {
      _widgets.add(ListTile(
        leading: Icon(item.icon()),
        title: Text(item.title()),
        onTap: () =>
          Navigator.push(
            context,
            FromBottomRoute(widget: MyDetailsPage(item))
          )
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).listTitle),
      ),
      body: Center(
        child: ListView(
          children: _widgets,
        )
      ),
    );
  }
}