import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:demo_app/localizables/messages_all.dart';

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get applicationTitle {
    return Intl.message(
        'Flutter List/Details',
        name: 'applicationTitle'
    );
  }

  String get listTitle {
    return Intl.message(
        'My List',
        name: 'listTitle'
    );
  }

  String get phoneTitle {
    return Intl.message(
      'Phone',
      name: 'phoneTitle'
    );
  }

  String get phoneDescription {
    return Intl.message(
        'A telephone, or phone, is a telecommunications device that permits two or more users to conduct a conversation when they are too far apart to be heard directly. A telephone converts sound, typically and most efficiently the human voice, into electronic signals that are transmitted via cables and other communication channels to another telephone which reproduces the sound to the receiving user.',
        name: 'phoneDescription'
    );
  }

  String get tabletTitle {
    return Intl.message(
        'Tablet',
        name: 'tabletTitle'
    );
  }

  String get tabletDescription {
    return Intl.message(
        'A tablet computer, commonly shortened to tablet, is a mobile device, typically with a mobile operating system and LCD touchscreen display processing circuitry, and a rechargeable battery in a single thin, flat package. Tablets, being computers, do what other personal computers do, but lack some I/O capabilities that others have. Modern tablets largely resemble modern smartphones, the only differences being that tablets are relatively larger than smartphones, with screens 7 inches (18 cm) or larger, measured diagonally, and may not support access to a cellular network.',
        name: 'tabletDescription'
    );
  }

  String get laptopTitle {
    return Intl.message(
        'Laptop',
        name: 'laptopTitle'
    );
  }

  String get laptopDescription {
    return Intl.message(
        'A laptop, also called a notebook computer or simply a notebook, is a small, portable personal computer with a "clamshell" form factor, having, typically, a thin LCD or LED computer screen mounted on the inside of the upper lid of the \"clamshell\" and an alphanumeric keyboard on the inside of the lower lid. The "clamshell" is opened up to use the computer. Laptops are folded shut for transportation, and thus are suitable for mobile use.[1] Its name comes from "lap", as it was deemed to be placed for use on a person\'s lap. Although originally there was a distinction between laptops and notebooks, the former being bigger and heavier than the latter, as of 2014, there is often no longer any difference.[2] Laptops are commonly used in a variety of settings, such as at work, in education, in playing games, Internet surfing, for personal multimedia and general home computer use.',
        name: 'laptopDescription'
    );
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) => AppLocalizations.load(locale);

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}